# PHP / Apache webstack

This is a derivate of the offical
[php:5.6-apache](https://registry.hub.docker.com/_/php/) image.

You can see the same instructions on:
[inserve/php](https://registry.hub.docker.com/u/inserve/php/)

This version adds the following capabilities to the container:

* Node.js v0.10.x (stable)
* Enables Zend OPcache
* Enable `mod_rewrite` for Apache
* Additional PHP extensions:
    * `iconv`
    * `mcrypt`
    * `soap`
    * `pdo`
    * `pdo_mysql`
    * `mysql`
    * `mysqli`
    * `gd`

To use this image, create a `Dockerfile` file in your project
and add this minimal setup:

    FROM inserve/php:5.6-apache
    EXPOSE 80

    # Add your sources (e.g. folder where your index.php is located )
    ADD src/ /var/www/html/

    # Change overnership of www-root to www-user
    RUN cd /var/www/html && chown -R www-data:www-data .


Then use the following commands to run the container with apache listening on `http://localhost:4567`

    $ sudo docker build -t myapp .
    $ sudo docker run --name=myapp_run -p 4567:80 myapp

To set custom Apache Server settings add these optional environment variables to your docker-compose.yml:

```
#!yml

environment:
    START_SERVERS: 7
    MIN_SPARE_SERVERS: 5
    MAX_SPARE_SERVERS: 10
    MAX_REQUEST_WORKERS: 41
```